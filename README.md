*Guia para Mapeo con KINECT*
================================

Dependencias:
-----------------------
- gstreamer1.0-tools 
- libgstreamer1.0-dev
- libgstreamer-plugins-base1.0-dev 
- libgstreamer-plugins-good1.0-dev
- gstreamer0.10-plugins-good

Nodos:
-----------------------
- gscam/gscam

Topics:
-----------------------
- image_raw [msg: sensor_msgs/Image]

Paso a paso
-----------------------
1. Conectar la videocamara al puerto y activar los permisos. Generalmente reconoce como video0

```
$ cd /dev/
$ ls -l
$sudo chmod 666 /dev/video0
```

2. Instalar dependencias de gscam

```
$ sudo apt-get install gstreamer1.0-tools 
$ sudo apt-get install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-good1.0-dev

```

3. Instalar package image_view (dependiendo de la versión de ROS)

```
$ sudo apt-get install ros-kinetic-image-view
```

4. Instalar package gscam

Existen dos alternativas para instalar el paquetede gscam, la primera es mediante el comando que dispone por defecto ros para la
instalacion dentro del workspace interno, y la segunda es mediante la clonacion y compilacion de dicho paquete dentro de un workspace 
creado manulamente mediante el comando catkin_make.

4.1 Metodo de instlacion por comano de ros:
```
$ sudo apt-get install ros-kinetic-gscam
```

4.2 Metodo de instalacion mediante clonacion en github, utilizando un workspace personal

```
cd catkin_ws
mkdir -p video_ws/src
cd video_ws
catkin_make
```
> Ahora se debe clonar el archivo desde github

```
$ cd src
$ git clone https://github.com/ros-drivers/gscam.git
$ cd ..
$ catkin_make
```
5. Instalar plugins adicionales

```
$ sudo apt-get install gstreamer0.10-plugins-good
```



Funcionamiento
------------------------
*Abrir 3 sesiones de terminal. En el primero se ejecuta roscore, en el segundo uno de los nodos de gscam y en el tercero uno de los nodos de image_view*

> En el primer terminal:

```
roscore
```

> En el segundo terminal:

```
$ roscd gscam
$ export GSCAM_CONFIG="v4l2src device=/dev/video0 ! video/x-raw-rgb,framerate=30/1 ! ffmpegcolorspace"

```

> En caso de que la cámara que se vaya a utilizar no esté como video0, ésta instrucción debe modificarse con el nombre correspondiente

> La cámara se enciende de la siguiente manera:

```
 $ rosrun gscam gscam
```

> Finalmente para mostrar la imagen en un tercer terminal:

```
$ rosrun image_view image_view image:=/camera/image_raw
```

Recomendaciones
-----------------------------

1. Antes de iniciar el proceso verificar que la cámara esté conectada y ubuntu la reconozca 
2. Revisar que la cámara está enviando información en el topic /camera/image_raw

```
$rostopic echo /camera/image_raw
```

3. Revisar el rqt_graph para comprobar su buen funcionamiento

```
rqt_graph
```
